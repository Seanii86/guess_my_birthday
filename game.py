from random import randint;
from time import sleep;
#  Prompt : guess the users birthday

#  First asks user with a prompt input player_name="Hello \U0001F609 What is your name?" and store it in var player_name.
#Using the random number generation for month_number, day_number, and year_number after the user inputs name,
#Start first guess with print: 'name, "we're you born on" + month_guess + day_guess +year_guess +"?""
#a second input prompt will then print, asking the user if 'Did I guess correctly? yes/no.lower'
#If user indicates 'yes', the response will print 'Woohoo!!!
#if user indicates it was incorrect 'no', The program will print: "Hmmm, let me try again"

player_name = input("Hello \U0001F609 What is your name? ").capitalize();

print("\nHello",player_name+",","I'm going to try and guess your birthday...")

sleep(1.0);

month_number = str(randint(1,12));
day_number = str(randint(1,31));
year_number = str(randint(1924,2004));

#Guesse 1

if month_number == "2" and day_number == "29" and year_number == "1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004":
     print("\nGuess 1:Okay",player_name+",","this is a longshot but...Were you born on",month_number+"/"+day_number+"/"+year_number+"? Aka a leapyear?");
elif month_number == "2" and day_number == "28":
    print("\nGuess 1:Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")
else:
     print("\nGuess 1: Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")

response = input("\nAm I correct? \U0001F60F yes/no ").lower();

if response == "yes":
    print("Woohoo!!! \U0001F600")
    exit()
else:
     print("\nDrat! let me try again.")
     
sleep(2.0);

#Guess 2

month_number = str(randint(1,12));
day_number = str(randint(1,31));
year_number = str(randint(1924,2004));

if month_number == "2" and day_number == "29" and year_number == "1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004":
     print("\nGuess 2:Okay",player_name+",","this is a longshot but...Were you born on",month_number+"/"+day_number+"/"+year_number+"? Aka a leapyear?");
elif month_number == "2" and day_number == "28":
    print("\nGuess 2:Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")
else:
     print("\nGuess 2: Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")

response = input("\nHow about this time? \U0001F60F yes/no ").lower();

if response == "yes":
    print("Woohoo!!! \U0001F600")
    exit()
else:
     print("\nHmm. Let me try a third time.")

sleep(2.0);

#Guess 3

month_number = str(randint(1,12));
day_number = str(randint(1,31));
year_number = str(randint(1924,2004));

if month_number == "2" and day_number == "29" and year_number == "1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004":
     print("\nGuess 3:Okay",player_name+",","this is a longshot but...Were you born on",month_number+"/"+day_number+"/"+year_number+"? Aka a leapyear?");
elif month_number == "2" and day_number == "28":
    print("\nGuess 3:Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")
else:
     print("\nGuess 3: Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")

response = input("\nDid I guess correctly this time? \U0001F60F yes/no ").lower();

if response == "yes":
    print("Woohoo!!! \U0001F600")
    exit()
else:
     print("\nNo?! Really?! Okay another.")

sleep(2.0);

# Guess 4

month_number = str(randint(1,12));
day_number = str(randint(1,31));
year_number = str(randint(1924,2004));

if month_number == "2" and day_number == "29" and year_number == "1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004":
     print("\nGuess 4:Okay",player_name+",","this is a longshot but...Were you born on",month_number+"/"+day_number+"/"+year_number+"? Aka a leapyear?");
elif month_number == "2" and day_number == "28":
    print("\nGuess 4:Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")
else:
     print("\nGuess 4: Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")

response = input("\nDid I guess your birthday? \U0001F60F yes/no ").lower();

if response == "yes":
    print("Woohoo!!! \U0001F600")
    exit()
else:
     print("\nOkay then, last try...")

sleep(2.0);

# Guess 5

month_number = str(randint(1,12));
day_number = str(randint(1,31));
year_number = str(randint(1924,2004));

if month_number == "2" and day_number == "29" and year_number == "1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004":
     print("\nGuess 5:Okay",player_name+",","this is a longshot but...Were you born on",month_number+"/"+day_number+"/"+year_number+"? Aka a leapyear?");
elif month_number == "2" and day_number == "28":
    print("\nGuess 5:Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")
else:
     print("\nGuess 5: Okay",player_name+", were you born on",month_number+"/"+day_number+"/"+year_number+"?")

response = input("\nWas I right? \U0001F60F yes/no ").lower();

if response == "yes":
    print("Woohoo!!! \U0001F600")
    exit()
else:
     print("\nI have other things to do. Good bye.")
     exit()
